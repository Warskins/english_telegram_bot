import logging

from aiogram import executor

from loader import dp, db
import middlewares, filters, handlers
from utils.notify_admins import on_startup_notify
from utils.set_bot_commands import set_default_commands
from utils.db_api.exceptions import DataBaseConnectionException

logger = logging.getLogger(__name__)


async def on_startup(dispatcher):
    # Создаем коннект к бд
    await db.connect()

    await db.get_user_subscriptions(319775014)
    # Устанавливаем дефолтные команды
    await set_default_commands(dispatcher)

    # Уведомляет про запуск
    await on_startup_notify(dispatcher)


if __name__ == '__main__':
    try:
        executor.start_polling(dp, on_startup=on_startup, skip_updates=True)
    except DataBaseConnectionException:
        logger.error("Can't connect to db.")
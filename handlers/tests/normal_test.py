import re

from aiogram import types
from aiogram.dispatcher import FSMContext

from keyboards.inline.generate_test_answers import generate_test_answers_inline_keyboard
from loader import dp, bot, db
from states.test_states import Test
from keyboards.default import static as default_static_keyboards


@dp.callback_query_handler(regexp=r"Test_(?P<test_id>\d+)", state=None)
async def start_normal_test(callback_query: types.CallbackQuery, regexp: re.Match, state: FSMContext):
    quiz_id = int(regexp.group("test_id"))
    question = await db.get_random_quiz_question(quiz_id)
    question_id = question['id']
    async with state.proxy() as data:
        data['quiz_id'] = quiz_id
        data['skip_questions_ids'] = [question_id]
        data['score'] = 0
        data['last_questions_id'] = question_id

    options = await db.get_question_options(question_id)
    question_options = [option['text'] for option in options]
    keyboard = generate_test_answers_inline_keyboard(question_options)
    await bot.send_message(callback_query.from_user.id, question['text'], reply_markup=keyboard)
    await Test.InProgress.set()


@dp.callback_query_handler(regexp=r"ans_(?P<answer>.+)", state=Test.InProgress)
async def process_normal_test(callback_query: types.CallbackQuery, regexp: re.Match, state: FSMContext):
    async with state.proxy() as data:
        current_question_id = data['last_questions_id']
        skip_questions_ids = data['skip_questions_ids']
        quiz_id = data['quiz_id']
    answer = regexp.group("answer")
    correct_answer = await db.get_question_correct_answer(current_question_id)
    if answer == correct_answer:
        await bot.send_message(callback_query.from_user.id, f'Правильно✅')
        async with state.proxy() as data:
            data['score'] += 1
    else:
        await bot.send_message(callback_query.from_user.id, f'Неправильно❌\nПравильный ответ: *{correct_answer}*' + '\n'
                               + '---------------------------------------------------------------' + '\n'
                                                                                                     'Чтобы отменить тест нажмите /cancel',
                               parse_mode='Markdown')
    next_question = await db.get_random_quiz_question(quiz_id, skip_questions_ids)
    if next_question:
        next_question_id = next_question['id']
        async with state.proxy() as data:
            data['skip_questions_ids'].append(next_question_id)
            data['last_questions_id'] = next_question_id

        options = await db.get_question_options(next_question_id)
        question_options = [option['text'] for option in options]
        keyboard = generate_test_answers_inline_keyboard(question_options)
        await bot.send_message(callback_query.from_user.id, next_question['text'], reply_markup=keyboard)
    else:
        async with state.proxy() as data:
            score = data['score']
            quiz_id = data['quiz_id']
        await db.save_user_result(callback_query.from_user.id, quiz_id, score)
        all_questions_count = len(skip_questions_ids)
        await bot.send_message(callback_query.from_user.id, f'Тест завершено. У вас {score} балов из {all_questions_count}')
        await state.finish()


@dp.message_handler(commands=['cancel'], state=Test.InProgress)
async def cancel_test(message: types.Message, state: FSMContext):
    await state.finish()
    await bot.send_message(message.from_user.id, 'Тест прерван', reply_markup=default_static_keyboards.Main_menu)

import re

from aiogram import types
from aiogram.dispatcher.filters import CommandStart

from keyboards.inline.quizzes_keyboard import generate_quizzes_keyboard
from loader import bot, dp, db
from keyboards.default import static as default_static_keyboards
from keyboards import texts


@dp.message_handler(commands=['start'])
async def command_start(message: types.Message):
    await db.register_user(message.from_user.id, message.from_user.username)
    await message.answer(texts.greetings, reply_markup=default_static_keyboards.Main_menu)


levels = ('Beginner(A1)👶', 'Pre-Intermediate(A2)🧒', 'Intermediate(B1)🧑‍🎓', 'Upper-Intermediate(B2)🧙')

LEVELS = {
    'A1': 1,
    'A2': 2,
    'B1': 3,
    'B2': 4
}
QUIZ_TYPES = {
    'Grammar': 1,
    'Reading': 2,
    'Listening': 3,
}

@dp.message_handler(lambda message: message.text and message.text in levels)
# @check_subscriptions
async def learning_programs(message: types.Message):
    if message.text == 'Beginner(A1)👶':
        await message.answer("Программа работы:", reply_markup=default_static_keyboards.MenuA1)
    elif message.text == 'Pre-Intermediate(A2)🧒':
        await message.answer("Программа работы:", reply_markup=default_static_keyboards.MenuA2)
    elif message.text == 'Intermediate(B1)🧑‍🎓':
        await message.answer("Программа работы:", reply_markup=default_static_keyboards.MenuB1)
    elif message.text == 'Upper-Intermediate(B2)🧙':
        await message.answer("Программа работы:", reply_markup=default_static_keyboards.MenuB2)


@dp.message_handler(regexp=r"(?P<type>\w+)\((?P<level>\w\d)\)")  # Reading A1 , Listening A1 , Grammar B1
async def process_test(message: types.Message, regexp: re.Match):
    quiz_type = regexp.group('type')
    quiz_level = regexp.group('level')
    quiz_type_id = QUIZ_TYPES.get(quiz_type)
    quiz_level_id = LEVELS.get(quiz_level)

    all_quizzes = await db.get_all_quizzes_by_type_and_level(quiz_level_id, quiz_type_id)
    keyboard = generate_quizzes_keyboard(all_quizzes, quiz_type_id)

    await bot.send_message(message.from_user.id, f'Програма работы {quiz_type} {quiz_level}', reply_markup=keyboard)





@dp.message_handler()
async def process_start_command(message: types.Message):
    if message.text == 'Программа работы🎓':
        await message.reply("Здесь вся теория и тесты:", reply_markup=default_static_keyboards.lvl_menu)
    elif message.text == 'О нас☎️':
        await bot.send_message(message.from_user.id, texts.developers)
    elif message.text == 'Ознакомиться с ботом👀':
        await message.answer("Отправляю видео:", reply_markup=default_static_keyboards.Main_menu)
    elif message.text == 'Main Menu🏠':
        await message.answer("Выберите опцию: ", reply_markup=default_static_keyboards.Main_menu)
    elif message.text == 'Информация о курсеℹ':
        await message.answer("Отправляю файл, ожидайте⏳:", reply_markup=default_static_keyboards.Main_menu)
        doc = open('staticfiles/Course.pdf', 'rb')
        await bot.send_document(message.chat.id, ('Информация.pdf', doc))
        doc.close()
    elif message.text == 'Назад':
        await message.answer("Выберите опцию: ", reply_markup=default_static_keyboards.lvl_menu)

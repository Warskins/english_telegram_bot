import re

from aiogram import types

from keyboards.inline.paymnet_keyboard import generate_payment_keyboard
from loader import dp, p2p, bot, db
from keyboards.default import static as default_static_keyboards
from keyboards.inline import static as inline_static_keyboards

from utils.pay import generate_bill


@dp.message_handler(text='Оплатить обучение💵')
async def process_pay_command(message: types.Message):
    await message.answer('Чтобы получить доступ к материалам, преобретите подписку, пожалуйста!'
                         '\nЕсли вас интересует покупка всего курса, напишите нашему преподователю @eng_trainee',
                         reply_markup=inline_static_keyboards.payment_keyboard)



@dp.callback_query_handler(regexp=r"pay_(?P<subscription_id>\d+)")
#@check_existing_subscription
async def send_payment(callback_query: types.CallbackQuery, regexp: re.Match):
    subscription_id = int(regexp.group("subscription_id"))
    subscription_price = await db.get_subscription_price(subscription_id)
    bill = await generate_bill(subscription_price)
    reply_keyboard = generate_payment_keyboard(bill.bill_id, subscription_id)
    await bot.send_message(callback_query.from_user.id, f'Ссылка для оплаты: \n {bill.pay_url}',
                           reply_markup=reply_keyboard)


@dp.callback_query_handler(regexp=r"pay_(?P<bill_id>.+)_(?P<subscription_id>\d+)")
async def check_payment(callback_query: types.CallbackQuery, regexp: re.Match):
    bill_id = regexp.group('bill_id')
    subscription_id = int(regexp.group('subscription_id'))
    bill = await p2p.check(bill_id=bill_id)
    bill_status = bill.status
    if bill_status == 'PAID':
        await bot.send_message(callback_query.from_user.id, 'Оплачено!')
        await db.add_user_subscription(subscription_id, user_id=callback_query.from_user.id)
    else:
        await bot.send_message(callback_query.from_user.id, 'Не оплачено')


@dp.callback_query_handler(regexp=r"cancel_pay_(?P<bill_id>.+)")
async def cancel_payment(callback_query: types.CallbackQuery, regexp: re.Match):
    bill_id = regexp.group('bill_id')
    await p2p.reject(bill_id=bill_id)
    await bot.send_message(callback_query.from_user.id, 'Оплата отменена', reply_markup=default_static_keyboards.Main_menu)

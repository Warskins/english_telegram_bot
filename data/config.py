from environs import Env

# Теперь используем вместо библиотеки python-dotenv библиотеку environs
env = Env()
env.read_env()

BOT_TOKEN = env.str("BOT_TOKEN")  # Забираем значение типа str
ADMINS = env.list("ADMINS")  # Тут у нас будет список из админов
IP = env.str("ip")  # Тоже str, но для айпи адреса хоста

MYSQL_HOST = env.str("MYSQL_HOST")
MYSQL_DATABASE = env.str("MYSQL_DATABASE")
MYSQL_USER = env.str("MYSQL_USER")
MYSQL_PASSWORD = env.str("MYSQL_PASSWORD")

QIWI_KEY = env.str("QIWI_PRIVATE_KEY")

import logging
from typing import List, Dict, Tuple

import aiomysql
from aiomysql import MySQLError

from utils.db_api.exceptions import DataBaseConnectionException

logger = logging.getLogger(__name__)


class DataBase:

    def __init__(self, host: str, user: str, password: str, db: str, loop):
        self.__host = host
        self.__user = user
        self.__password = password
        self.__db = db
        self.__loop = loop
        self.__pool = None

    async def connect(self) -> None:
        try:
            self.__pool = await aiomysql.create_pool(host=self.__host,
                                                     port=3306,
                                                     user=self.__user,
                                                     password=self.__password,
                                                     db=self.__db,
                                                     autocommit=True)
        except MySQLError as e:
            logger.error(f'Database Connection Error: {e}', exc_info=False)
            raise DataBaseConnectionException

    async def __insert(self, table_name: str, column_values: dict = None, **kwargs) -> int:
        try:
            async with self.__pool.acquire() as conn:
                async with conn.cursor(aiomysql.DictCursor) as cursor:
                    if column_values is None:
                        column_values = kwargs
                    columns = ', '.join(column_values.keys())
                    values = [tuple(column_values.values())]
                    placeholders = '%s, ' * len(column_values.keys())
                    placeholders = placeholders.rstrip(', ')
                    sql_statement = f"INSERT INTO {table_name} ({columns}) VALUES ({placeholders})"
                    await cursor.executemany(sql_statement, values)
                    return cursor.lastrowid
        except MySQLError as e:
            logger.error(f'Database Error: {e} \nTrying to reconnect')
            await self.connect()
        except Exception as e:
            logger.error(f'Python Exception: {e} \nTrying to reconnect')
            await self.connect()

    async def __fetch_all(self, sql_statement: str, params: tuple = None) -> tuple:
        try:
            async with self.__pool.acquire() as conn:
                async with conn.cursor(aiomysql.DictCursor) as cursor:
                    await cursor.execute(sql_statement, params)
                    return await cursor.fetchall()
        except MySQLError as e:
            logger.error(f'Database Error: {e} \nTrying to reconnect')
            await self.connect()
        except Exception as e:
            logger.error(f'Python Exception: {e} \nTrying to reconnect')
            await self.connect()

    async def __fetch_one(self, sql_statement: str, params: tuple = None) -> Dict:
        try:
            async with self.__pool.acquire() as conn:
                async with conn.cursor(aiomysql.DictCursor) as cursor:
                    await cursor.execute(sql_statement, params)
                    return await cursor.fetchone()
        except MySQLError as e:
            logging.exception(f'Database Error: {e} \nTrying to reconnect')
            await self.connect()
        except Exception as e:
            logging.exception(f'Python Exception: {e} \nTrying to reconnect')
            await self.connect()

    async def get_user_subscriptions(self, user_id: int) -> List[str]:
        sql_query = "SELECT s.name FROM telegram_user_subscriptions JOIN subscription s on s.id = " \
                    "telegram_user_subscriptions.subscription_id WHERE telegramuser_id = %s;"
        user_subscriptions = await self.__fetch_all(sql_query, (user_id,))
        return [user_subscription['name'] for user_subscription in user_subscriptions]

    async def get_random_quiz_question(self, quiz_id: int, skip_questions_ids=None) -> Dict:
        if skip_questions_ids is None:
            skip_questions_ids = [0]
        #skip_questions_ids = [str(_id) for _id in skip_questions_ids]
        format_strings = ','.join(['%s'] * len(skip_questions_ids))

        #skip_questions_ids_str = ",".join(skip_questions_ids)
        sql_query = f"SELECT id, text, weight FROM quiz_question where quiz_id=%s" \
                    f" AND id not in ({format_strings}) ORDER BY RAND() LIMIT 1;"
        question = await self.__fetch_one(sql_query, (quiz_id, *skip_questions_ids))
        return question

    async def get_question_correct_answer(self, question_id: int) -> str:
        sql_query = "SELECT text from quiz_answer WHERE question_id = %s and correct = 1;"
        result = await self.__fetch_one(sql_query, (question_id,))
        return result['text']

    async def get_question_options(self, question_id: int) -> List[Dict]:
        sql_query = "SELECT text, correct FROM quiz_answer WHERE question_id=%s"
        options = await self.__fetch_all(sql_query, (question_id,))
        return list(options)

    async def save_user_result(self, user_id: int, quiz_id: int, result: int = 0) -> None:
        user_result = {'score': result, 'quiz_id': quiz_id, 'user_id': user_id}
        await self.__insert('user_result', user_result)

    async def set_user_level(self, level: int, user_id: int) -> None:
        sql_query = "UPDATE telegram_user SET level = %s WHERE user_id = %s;"
        await self.__fetch_one(sql_query, (level, user_id))

    async def register_user(self, user_id: int, user_nickname: str = None) -> bool:
        sql_query = "SELECT user_id FROM telegram_user WHERE user_id = %s"
        result = await self.__fetch_one(sql_query, (user_id, ))
        if not result:
            user_info = {'user_id': user_id, 'nickname': user_nickname}
            await self.__insert('telegram_user', user_info)
            return True
        return False

    async def get_all_quizzes_by_type_and_level(self, level_id: int , quiz_type_id: int) -> List[Dict]:
        sql_query = "SELECT name , id FROM quiz WHERE level_id = %s and type_id = %s"
        rows = await self.__fetch_all(sql_query, (level_id,quiz_type_id))
        return list(rows)

    async def get_subscription_price(self, subscription_id: int) -> int:
        sql_query = "SELECT price FROM subscription WHERE id = %s"
        row = await self.__fetch_one(sql_query, (subscription_id, ))
        return row['price']

    async def add_user_subscription(self, subscription_id: int, user_id: int) -> None:
        context = {'telegramuser_id': user_id, 'subscription_id': subscription_id}
        await self.__insert('telegram_user_subscriptions', context)

    # async def register_user(self, user_data: dict):
    #     await self.__insert('manager_telegramuser', user_data)
    #
    # async def check_user_exists_in_db(self, user_id: int) -> bool:
    #     sql_statement = 'SELECT COUNT(telegram_id) FROM manager_telegramuser WHERE telegram_id = %s'
    #     user_count = await self.__execute_all(sql_statement, (user_id,))
    #     # Возвращается кортеж с одним елементом, тоже кортежем. Берем первый елемент, внутри которого находится
    #     # счетчик значения
    #     return user_count[0][0] == 1
    #
    # async def get_banned_users(self) -> tuple:
    #     sql_statement = 'SELECT telegram_id from manager_telegramuser WHERE blocked=1;'
    #     banned_users = await self.__execute_all(sql_statement)
    #     return tuple([user[0] for user in banned_users])
    #
    # async def get_user_status(self, user_id: int) -> tuple:
    #     sql_statement = 'SELECT mt.name FROM manager_telegramuser JOIN manager_telegramuserstatus mt on mt.id = ' \
    #                     'manager_telegramuser.status_id WHERE telegram_id = %s; '
    #     (user_status,) = await self.__execute_one(sql_statement, (user_id,))
    #     return user_status
    #
    # async def update_user_status(self, user_id: int, new_status_id: int) -> None:
    #     sql_statement = 'UPDATE manager_telegramuser SET status_id=%s WHERE telegram_id=%s;'
    #     await self.__execute_one(sql_statement, (new_status_id, user_id))
    #
    # async def check_user_ban(self, user_id: int) -> bool:
    #     sql_statement = 'SELECT blocked FROM manager_telegramuser WHERE telegram_id = %s;'
    #     blocked, = await self.__execute_one(sql_statement, (user_id,))
    #     return blocked == 1
    #
    # async def get_status_id_by_name(self, status_name: str) -> int:
    #     sql_statement = 'SELECT id FROM manager_telegramuserstatus WHERE name = %s;'
    #     status_id, = await self.__execute_one(sql_statement, (status_name,))
    #     return status_id
    #
    # async def get_help_types(self) -> tuple:
    #     sql_statement = 'SELECT * FROM manager_helptype'
    #     return await self.__execute_all(sql_statement)
    #
    # async def get_all_sections(self) -> tuple:
    #     sql_statement = 'SELECT name FROM manager_section'
    #     all_sections = await self.__execute_all(sql_statement)
    #     self.all_sections = tuple([section[0] for section in all_sections])
    #     await self.update_all_units()
    #     return all_sections
    #
    # async def get_units_by_section_name(self, section_name: str) -> tuple:
    #     sql_statement = 'SELECT manager_unit.name FROM manager_unit JOIN manager_section ms on ms.id = ' \
    #                     'manager_unit.section_id WHERE ms.name = %s; '
    #     units = await self.__execute_all(sql_statement, (section_name,))
    #     return units
    #
    # async def get_materials_without_unit_by_section_name(self, section_name: str) -> tuple:
    #     sql_statement = 'SELECT manager_material.id, manager_material.name, manager_material.description, ' \
    #                     'mf.id, mf.file_abs_path, mf.telegram_file_id, manager_material.url FROM manager_material  ' \
    #                     'JOIN manager_section ms on ms.id = manager_material.section_id LEFT OUTER JOIN  ' \
    #                     'manager_file mf on manager_material.file_id = mf.id WHERE unit_id IS NULL AND ms.name = %s; '
    #     materials = await self.__execute_all(sql_statement, (section_name,))
    #     return materials
    #
    # async def get_materials_by_unit_name(self, unit_name: str) -> tuple:
    #     sql_statement = "SELECT manager_material.id, manager_material.name, manager_material.description, " \
    #                     "mf.id, mf.file_abs_path, mf.telegram_file_id, manager_material.url FROM manager_material " \
    #                     "JOIN manager_unit mu on mu.id = manager_material.unit_id LEFT OUTER JOIN manager_file mf on " \
    #                     "manager_material.file_id = mf.id WHERE mu.name = %s; "
    #     materials = await self.__execute_all(sql_statement, (unit_name,))
    #     return materials
    #
    # async def get_material_name_by_id(self, material_id: int) -> str:
    #     sql_statement = 'SELECT name FROM manager_material WHERE id = %s;'
    #     material_name, = await self.__execute_one(sql_statement, (material_id,))
    #     return material_name
    #
    # async def get_material_comments(self, material_id: int) -> tuple:
    #     sql_statement = 'SELECT text, creation_date FROM manager_comment WHERE material_id = %s;'
    #     comments = await self.__execute_all(sql_statement, (material_id,))
    #     return comments
    #
    # async def add_material_comment(self, material_id: int, user_id: int, text: str) -> None:
    #     await self.__insert('manager_comment', material_id=material_id, user_id=user_id, text=text)
    #
    # async def update_file_telegram_id(self, file_id: int, new_telegram_file_id: int) -> int:
    #     sql_statement = 'UPDATE manager_file SET telegram_file_id=%s WHERE id= %s'
    #     await self.__execute_all(sql_statement, (new_telegram_file_id, file_id))
    #
    # async def update_number_of_section_views(self, section_name: str) -> None:
    #     sql_statement = 'UPDATE manager_section SET views= views + 1 WHERE name= %s;'
    #     await self.__execute_all(sql_statement, (section_name,))
    #
    # async def get_random_expert_by_help_type(self, help_type: int) -> tuple:
    #     sql_statement = 'SELECT first_name, last_name, telegram_id FROM manager_expert JOIN manager_expert_help_type ' \
    #                     'meht on manager_expert.telegram_id = meht.expert_id WHERE meht.helptype_id = %s ORDER BY ' \
    #                     'RAND() LIMIT 1; '
    #     try:
    #         expert, = await self.__execute_all(sql_statement, (help_type,))
    #         return expert
    #     except Exception:
    #         return tuple()
    #
    # async def create_help_ticket(self, user_id: int, expert_id: int, help_type: int) -> int:
    #     ticket_info = {'expert_id': expert_id,
    #                    'help_type_id': help_type,
    #                    'user_id': user_id}
    #
    #     ticket_id = await self.__insert('manager_helpticket', ticket_info)
    #     return ticket_id
    #
    # async def save_help_ticket_comment(self, ticket_id: int, comment: str) -> None:
    #     sql_statement = 'UPDATE manager_helpticket SET expert_comment = %s WHERE id = %s;'
    #     await self.__execute_one(sql_statement, (comment, ticket_id))
    #
    # async def create_bullying_ticket(self, user_id: int, message: str, user_contact: str):
    #     ticket_info = {'user_id': user_id,
    #                    'message': message,
    #                    'user_contact': user_contact}
    #     await self.__insert('manager_bullyingticket', ticket_info)

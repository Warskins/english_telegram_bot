import re

from aiogram import types

from loader import bot, db

LEVELS = {
    1: 'A1',
    2: 'A2',
    3: 'B1',
    4: 'B2'
}


def get_user_level(score: int) -> tuple:
    if score <= 15:
        level_int = 1
    elif score <= 25:
        level_int = 2
    elif score <= 35:
        level_int = 3
    else:
        level_int = 4
    level_str = LEVELS.get(level_int)
    return level_int, level_str


def parse_subscription_from_message(message_text: str) -> str:
    subscription = re.search(r'\((.*?)\)', message_text).group(1)
    return subscription


async def check_request_subscription(message: types.Message) -> bool:
    user_id = message.from_user.id
    user_subscriptions = await db.get_user_subscriptions(user_id)
    request_subscription = parse_subscription_from_message(message.text)
    if request_subscription in user_subscriptions:
        return True
    return False


def check_subscriptions(func):
    async def wrapper(message: types.Message):
        if not await check_request_subscription(message):
            return await bot.send_message(message.from_user.id, 'Чтобы получить дотсуп,приобретите нужный вам курс😊')
        return await func(message)

    return wrapper


def check_existing_subscription(func):
    async def wrapper(callback_query: types.CallbackQuery, regexp: re.Match):
        user_id = callback_query.from_user.id
        user_subscriptions = await db.get_user_subscriptions(user_id)
        request_subscription_id = int(regexp.group("subscription_id"))
        request_subscription_name = LEVELS.get(request_subscription_id, '')
        if request_subscription_name in user_subscriptions:
            return await bot.send_message(callback_query.from_user.id, 'Данная подписка уже приобретена✔️')
        return await func(callback_query)

    return wrapper

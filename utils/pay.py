from datetime import datetime, timedelta

from pyqiwip2p.p2p_types import Bill

from loader import p2p


async def generate_bill(amount: int) -> Bill:
    expired = datetime.now() + timedelta(minutes=10)
    new_bill = await p2p.bill(amount=amount, expiration=expired)
    return new_bill

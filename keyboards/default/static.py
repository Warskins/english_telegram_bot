# -------------menu#1 the main menu ------------------#
from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

button_name = KeyboardButton('Программа работы🎓')
button_contacts = KeyboardButton('О нас☎️')
button_test = KeyboardButton('Определить уровень❓')
button_payments = KeyboardButton('Оплатить обучение💵')
check_the_bot = KeyboardButton('Ознакомиться с ботом👀')
button_info = KeyboardButton('Информация о курсеℹ')
Main_menu = ReplyKeyboardMarkup(resize_keyboard=True).add(button_name, button_contacts, button_test , button_payments, check_the_bot, button_info)    #(location, contact)
# -------------menu#1 the main menu ------------------#



# ------------------ lvl menu ----------------------#
button_a1 = KeyboardButton("Beginner(A1)👶")
button_a2 = KeyboardButton("Pre-Intermediate(A2)🧒")
button_b1 = KeyboardButton("Intermediate(B1)🧑‍🎓")
button_b2 = KeyboardButton("Upper-Intermediate(B2)🧙")
button_back =KeyboardButton("Main Menu🏠")
lvl_menu = ReplyKeyboardMarkup(resize_keyboard=True, row_width=2).add(button_a1,button_a2, button_b1, button_b2, button_back)
# ------------------ lvl menu ----------------------#


# ------------------ The course A1 menu ----------------------#
button_grammarA1 = KeyboardButton('Grammar(A1)')             # название кнопки
button_backA1 = KeyboardButton('Назад')
button_listeningA1 = KeyboardButton('Listening(A1)')
button_vocabularyA1 = KeyboardButton('Vocabulary(A1)')
button_readingA1 = KeyboardButton('Reading(A1)')
MenuA1 = ReplyKeyboardMarkup(resize_keyboard=True).add(button_grammarA1, button_readingA1, button_listeningA1, button_vocabularyA1 , button_backA1)
# ------------------ The course A1 menu ----------------------#


# ------------------ The course A2 menu ----------------------#
button_grammarA2 = KeyboardButton('Grammar(A2)')             # название кнопки
button_backA2 = KeyboardButton('Назад')
button_listeningA2 = KeyboardButton('Listening(A2)')
button_vocabularyA2 = KeyboardButton('Vocabulary(A2)')
button_readingA2 = KeyboardButton('Reading(A2)')
MenuA2 = ReplyKeyboardMarkup(resize_keyboard=True).add(button_grammarA2, button_readingA2, button_listeningA2, button_vocabularyA2 , button_backA2)
# ------------------ The course A2 menu ----------------------#


# ------------------ The course B1 menu ----------------------#
button_grammarB1 = KeyboardButton('Grammar(B1)')             # название кнопки
button_backB1 = KeyboardButton('Назад')
button_listeningB1 = KeyboardButton('Listening(B1)')
button_vocabularyB1 = KeyboardButton('Vocabulary(B1)')
button_readingB1 = KeyboardButton('Reading(B1)')
MenuB1 = ReplyKeyboardMarkup(resize_keyboard=True).add(button_grammarB1, button_readingB1, button_listeningB1, button_vocabularyB1 , button_backB1)
# ------------------ The course B1 menu ----------------------#

# ------------------ The course B2 menu ----------------------#
button_grammarB2 = KeyboardButton('Grammar(B2)')             # название кнопки
button_backB2 = KeyboardButton('Назад')
button_listeningB2 = KeyboardButton('Listening(B2)')
button_vocabularyB2 = KeyboardButton('Vocabulary(B2)')
button_readingB2 = KeyboardButton('Reading(B2)')
MenuB2 = ReplyKeyboardMarkup(resize_keyboard=True).add(button_grammarB2, button_readingB2, button_listeningB2, button_vocabularyB2 , button_backB2)
# ------------------ The course B2 menu ----------------------#



keyboards = []

# --------------------A1_grammar--------------------#
A1_inlineKeyboard1= InlineKeyboardButton('To be', callback_data='Test_45')
A1_inlineKeyboard2= InlineKeyboardButton('Question Word Order', callback_data='Test_46')
A1_inlineKeyboard3= InlineKeyboardButton('Nouns', callback_data='Test_47')
A1_inlineKeyboard4= InlineKeyboardButton('Present Simple', callback_data='Test_48')
A1_inlineKeyboard5= InlineKeyboardButton('Pronouns', callback_data='Test_49')
A1_inlineKeyboard6= InlineKeyboardButton('Present Continious', callback_data='Test_50')
A1_inlineKeyboard7= InlineKeyboardButton('Articles', callback_data='Test_51')
A1_inlineKeyboard8= InlineKeyboardButton('Past Simple', callback_data='Test 52')
A1_inlineKeyboard9= InlineKeyboardButton('Prepositions', callback_data='Test 53')
A1_inlineKeyboard10= InlineKeyboardButton('Demonstratives', callback_data='Test 54')
#A1_inlineKeyboard11= InlineKeyboardButton('Possessive case', callback_data='Test 55') -no tests!
A1_inlineKeyboard12= InlineKeyboardButton('There is/There are', callback_data='Test 56')
A1_inlineKeyboard13= InlineKeyboardButton('Future Simple', callback_data='Test 57')
A1_inlineKeyboard14= InlineKeyboardButton('Modal Verbs', callback_data='Test 58')
A1_inlineKeyboard16= InlineKeyboardButton('Adverbs of Frequency', callback_data='Test 59')
A1_inlineKeyboard17= InlineKeyboardButton('Adverbs of manner', callback_data='Test 60')
A1_get_konspekt = InlineKeyboardButton('Получить конспект' , callback_data='Get A1')

"""
A1_inlineKeyboard11, 
"""
#####################################################################################################
A1_GrammarFull=InlineKeyboardMarkup(row_width=2).add(A1_inlineKeyboard1,A1_inlineKeyboard2,A1_inlineKeyboard3,A1_inlineKeyboard4,A1_inlineKeyboard5,A1_inlineKeyboard6
,A1_inlineKeyboard7,A1_inlineKeyboard8,A1_inlineKeyboard9,A1_inlineKeyboard10,A1_inlineKeyboard12,A1_inlineKeyboard13,A1_inlineKeyboard14
,A1_inlineKeyboard16,A1_inlineKeyboard17, A1_get_konspekt)


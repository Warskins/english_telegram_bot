from typing import List

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


def generate_test_answers_inline_keyboard(options: List) -> InlineKeyboardMarkup:
    keyboard = InlineKeyboardMarkup()
    for option in options:
        btn = InlineKeyboardButton(option, callback_data=f'ans_{option}')
        keyboard.add(btn)
    return keyboard

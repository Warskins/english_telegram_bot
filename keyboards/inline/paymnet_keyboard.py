from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


def generate_payment_keyboard(bill_id: int, subscription_id: int) -> InlineKeyboardMarkup:
    check_button = InlineKeyboardButton('Проверить оплату', callback_data=f'check_pay_{bill_id}_{subscription_id}')
    cancel_button = InlineKeyboardButton('Отменить оплату', callback_data=f'cancel_pay_{bill_id}')
    keyboard = InlineKeyboardMarkup(row_width=2).add(check_button, cancel_button)
    return keyboard

from typing import Dict, List

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


def generate_quizzes_keyboard(quizzes: List[Dict], quiz_type: int) -> InlineKeyboardMarkup:
    if quiz_type == 1:
        buttons = [InlineKeyboardButton(quiz['name'], callback_data=f"Test_{quiz['id']}") for quiz in quizzes]
    elif quiz_type == 2:
        buttons = [InlineKeyboardButton(quiz['name'], callback_data=f"Topic_{quiz['id']}") for quiz in quizzes]
    elif quiz_type == 3:
        buttons = [InlineKeyboardButton(quiz['name'], callback_data=f"Listening_{quiz['id']}") for quiz in quizzes]
    keyboard = InlineKeyboardMarkup(row_width=2).add(*buttons)
    return keyboard

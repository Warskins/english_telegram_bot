# payment
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

a1_paymentButton = InlineKeyboardButton('A1', callback_data='pay_1')
a2_paymentButton = InlineKeyboardButton('A2', callback_data='pay_2')
b1_paymentButton = InlineKeyboardButton('B1', callback_data='pay_3')
b2_paymentButton = InlineKeyboardButton('B2', callback_data='pay_4')
# full_course_paymentButton = InlineKeyboardButton('Весь курс', callback_data='pay/5')


payment_keyboard = InlineKeyboardMarkup(resize_keyboard=True).add(a1_paymentButton,
                                                                  a2_paymentButton, b1_paymentButton,
                                                                  b2_paymentButton)

import asyncio

from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from pyqiwip2p import AioQiwiP2P

from data import config
from utils.db_api import DataBase

loop = asyncio.get_event_loop()
bot = Bot(token=config.BOT_TOKEN, parse_mode=types.ParseMode.HTML)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

db = DataBase(
    host=config.MYSQL_HOST,
    user=config.MYSQL_USER,
    password=config.MYSQL_PASSWORD,
    db=config.MYSQL_DATABASE,
    loop=loop
)

p2p = AioQiwiP2P(auth_key=config.QIWI_KEY)
